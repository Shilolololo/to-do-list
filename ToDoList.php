<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To-Do List</title>
</head>
<body>
    <?php
    session_start();   #Debut de la session

    if (!isset($_SESSION['tasks'])) {     #Création du tableau de la session avec if/isset et array pour creer une pile des valeurs rentrées
        $_SESSION['tasks'] = array();
    }

    if (isset($_POST['add_task'])) {        #ajoute la valeur dans la variable $_POST['task'] au tableau de la session.
        array_push($_SESSION['tasks'], $_POST['task']);   #Pour empiler les valeurs rentrées
    }
    ?>

    <h2>To-Do List</h2>  
    <ul>
        <?php foreach ($_SESSION['tasks'] as $index => $task) :   #boucle pour parcourir le tableau de tâches "$task" stockées dans la session
         ?>  
            <li><?=$task?></li>
        <?php endforeach; ?>
    </ul>   

    <h3>Ajouter une tâche</h3>  
    <form method="post">
        <input type="text" name="task" placeholder="Nouvelle tâche">  <!-- Pour indiquer ce qui est attendu dans l'interface pour recuperer et empiler les données -->
        <input type="submit" name="add_task" value="Ajouter">           <!-- Pour soumettre la donnée dans le tableau -->
    </form>   
</body>
</html>

